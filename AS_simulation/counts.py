import numpy as np

from AS_simulation.utils import invlogit


def sample_total_counts(n_events, n_samples, expected_counts, sigma=1):
    mu = np.log(expected_counts) - sigma ** 2 / 2
    lambda_events = np.exp(np.random.normal(mu, sigma, size=n_events))
    lambda_events = np.vstack([lambda_events] * n_samples).transpose()
    total = np.random.poisson(lambda_events)
    return(total)


def get_AS_reads(expected_counts, X=None, psi=None):
    if psi is None:
        if X is None:
            msg = 'Either logit or psi must be provided for simulating reads'
            raise ValueError(msg)
        psi = invlogit(X)
        psi[np.isnan(psi)] = 1
    
    if len(psi.shape) > 1:
        n_samples = psi.shape[1]
    else:
        n_samples = 1
        psi = np.reshape(psi, (psi.shape[0], 1))
    n_events = psi.shape[0]
    
    total = sample_total_counts(n_events, n_samples, expected_counts)
    inclusion = np.random.binomial(total, psi)
    
    return(inclusion, total)


def get_AS_reads_uniform(X, loglambda):
    psi = np.exp(X) / (1 + np.exp(X))
    total = np.random.poisson(np.exp(loglambda), size=X.shape)
    inclusion = np.random.binomial(total, psi)
    return(inclusion, total)
