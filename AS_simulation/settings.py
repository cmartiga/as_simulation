from os.path import join, abspath, dirname

BASE_DIR = abspath(join(dirname(__file__), '..'))
DEF_RUNFILE = join(BASE_DIR, 'config', 's_8_4x.runfile')
TEST_DATA_DIR = join(BASE_DIR, 'test', 'test_data')
BIN_DIR = join(BASE_DIR, 'bin')

VERSION = '0.1.1'
