from itertools import combinations

import numpy as np

from AS_simulation.utils import logit, invlogit
import random


def simulate_baseline_psi(n_events, p_alt=0.2, logit_scale=True):
    n_alt = int(n_events * p_alt)
    psi = np.append(np.random.uniform(size=(1, n_alt)),
                    np.random.beta(10, 1, size=(1, n_events - n_alt)))
    if logit_scale:
        return(logit(psi))
    return(psi)


def simulate_gene_txs_psi(logit_mean, logit_sd, n_txs):
    '''
       Simulates proportions of transcripts assuming only potential
       single exon skipping event happening with a low probability.
       Whever the alternative transcripts reach a proportion of 1, both
       main and remaining transcripts have 0 proportion

       logit_mean and logit_sd are the mean and standard deviation of the
       individual exon inclusion rates
       '''
    if n_txs == 1:
        return(np.array([1]))

    psis = []
    for _ in range(n_txs - 1):
        p = 1 / (1 + np.exp(np.random.normal(logit_mean, logit_sd)))
        p = min(p, 1 - sum(psis) - p)
        if p < 0:
            psis.append(0)
        else:
            psis.append(p)
    psis = np.append([1 - sum(psis)], [psis])
    return(psis)


def simulate_exon_psi(logit_mean, logit_sd, p_alternative, n_exons,
                      max_alternative=None, alternative_idxs=None):
    psi = np.full(n_exons, 1.)
    
    # Truncate number skipping events per gene
    n_alternative = np.random.binomial(max(n_exons - 2, 0), p_alternative)
    if max_alternative is not None and n_alternative > max_alternative:
        n_alternative = max_alternative
        
    idxs = list(range(1, n_exons - 1))
    alternative = random.choices(idxs, k=n_alternative)
    if alternative_idxs is not None:
        alternative = np.unique(alternative + alternative_idxs)
        n_alternative = alternative.shape[0]
    if n_alternative > 0:
        psi[alternative] = invlogit(np.random.normal(logit_mean, logit_sd,
                                                     size=n_alternative))
    return(psi)


def tx_psi_to_exon_psi(txs, psis):
    return(np.dot(psis, txs))


def exon_psi_to_txs_psi(exon_psi, max_skipped=3):
    ''''Calculates transcripts PSIs from given exon PSIs

        It assumes independent exon inclusion rates along the transcript
        such that combination of skipping events can be calculated
        by multiplying the marginal probabilities for each exon.

        As the number of alternative exons increases, the number of
        combinations increases very rapidly, so we limit to a maximum
        of N exons being skipped at the same time

        '''
    n_exons = exon_psi.shape[0]
    alternative_exons = np.where(np.logical_and(exon_psi != 1,
                                                exon_psi != 0))[0]
    n_alternative = alternative_exons.shape[0]

    txs = []
    txs_psi = []
    for n_exons_skipped in range(min(n_alternative, max_skipped) + 1):
        for skipped_exons in combinations(alternative_exons, n_exons_skipped):
            skipped_exons = np.array(skipped_exons)
            tx_exons = np.full(n_exons, True)
            if skipped_exons.shape[0] > 0:
                tx_exons[skipped_exons] = False

            psi = np.prod(exon_psi[tx_exons])
            if skipped_exons.shape[0] > 0:
                psi = psi * np.prod(1 - exon_psi[skipped_exons])

            txs.append(tx_exons)
            txs_psi.append(psi)

    txs = np.vstack(txs)
    txs_psi = np.array(txs_psi)

    # Fill missing txs with the full length
    txs_psi[0] += 1 - txs_psi.sum()
    return(txs, txs_psi)
