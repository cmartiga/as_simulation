import numpy as np

from AS_simulation.utils import extract_tx_seq, get_exon_seqs
from AS_simulation.psi import (exon_psi_to_txs_psi, simulate_exon_psi,
                               tx_psi_to_exon_psi)


def get_gene_tpms(gene_id, log_mean, log_sd, tpms=None):
    if tpms is None:
        gene_tpms = np.exp(np.random.normal(log_mean, log_sd))
    else:
        gene_tpms = tpms.get(gene_id, 0)
    return(gene_tpms)


def _format_exon_id(chrom, strand, exon):
    start, end = exon
    return('{}:{}-{}{}'.format(chrom, start, end, strand))


def get_exons_longest_tx(txs, strand):
    longest_tx = max(txs, key=lambda x: len(txs[x]))
    gene_exons = txs[longest_tx]
    if strand == '-':
        gene_exons = gene_exons[::-1]
    return(gene_exons)


def _get_exons_psi(logit_mean, logit_sd, p_alternative, exon_ids,
                   max_alternative, psi_dict=None, alternative_idxs=None):
    n_exons = len(exon_ids)
    
    if psi_dict is None:
        exons_psi = simulate_exon_psi(logit_mean, logit_sd,
                                      p_alternative=p_alternative,
                                      n_exons=n_exons,
                                      max_alternative=max_alternative,
                                      alternative_idxs=alternative_idxs)
    else:
        exons_psi = np.array([psi_dict.get(exon_id, 1)
                              for exon_id in exon_ids])
    return(exons_psi)


def get_exon_ids(chrom, strand, exons):
    exon_ids = [_format_exon_id(chrom, strand, exon) for exon in exons]
    return(exon_ids)


def get_alternative_idxs(exon_ids, alternative_exons=None):
    alternative_idxs = None
    if alternative_exons is not None:
        alternative_idxs = [i for i, exon_id in enumerate(exon_ids)
                            if exon_id in alternative_exons]
    return(alternative_idxs) 


def sim_txs_quant(gene_data, logit_mean, logit_sd, p_alternative,
                  max_alternative=3, max_skipped=3, psi_dict=None,
                  alternative_exons=None):
    
    txs = gene_data.get('txs', {})
    chrom, strand = gene_data['chrom'], gene_data['strand']
    exons = get_exons_longest_tx(txs, strand)
    exon_ids = get_exon_ids(chrom, strand, exons)
    alternative_idxs = get_alternative_idxs(exon_ids,
                                            alternative_exons=alternative_exons)
    exons_psi = _get_exons_psi(logit_mean, logit_sd, p_alternative,
                               exon_ids, psi_dict=psi_dict,
                               max_alternative=max_alternative,
                               alternative_idxs=alternative_idxs)
    txs, psis = exon_psi_to_txs_psi(exons_psi, max_skipped)
    exon_psi_mod = tx_psi_to_exon_psi(txs, psis)
    
    return({'exon_psi': exons_psi, 'txs': txs,
            'tx_psi': psis, 'exon_ids': exon_ids,
            'exons': exons, 'exon_psi_mod': exon_psi_mod})


def simulate_txs_quant(parsed_gff, genome, log_mean=4, log_sd=1,
                       logit_mean=0, logit_sd=1, tpms=None,
                       psi_dict=None, max_skipped=3,
                       max_alternative=3, p_alternative=0.1,
                       alternative_exons=None):

    for gene_id, gene_data in parsed_gff.items():
        tx_quant = sim_txs_quant(gene_data, logit_mean, logit_sd,
                                 p_alternative, max_alternative=max_alternative,
                                 max_skipped=max_skipped,
                                 psi_dict=psi_dict,
                                 alternative_exons=alternative_exons)
        tx_quant['exon_seqs'] = get_exon_seqs(tx_quant['exons'],
                                              gene_data['chrom'],
                                              gene_data['strand'], genome)
        tx_quant['gene_id'] = gene_data['gene_id']
        tx_quant['tpm'] = get_gene_tpms(gene_id, log_mean, log_sd, tpms)
        yield(tx_quant)


def get_tx_data(tx_quant, min_tpms=0.001):
    for i, (tx_exons, psi) in enumerate(zip(tx_quant['txs'],
                                            tx_quant['tx_psi'])):
        tx_id = '{}.{}'.format(tx_quant['gene_id'], i)
        tx_tpms = tx_quant['tpm'] * psi
        if tx_tpms < min_tpms:
            continue
        exons = [e for e, take in zip(tx_quant['exons'], tx_exons) if take]
        seq = extract_tx_seq(exons, tx_quant['exon_seqs'])
        if not seq:
            continue
        record = {'tx_id': tx_id, 'seq': seq, 'psi': psi, 'tpm': tx_tpms}
        yield(record)
