from subprocess import Popen, PIPE
from tempfile import NamedTemporaryFile
import argparse
import sys

from AS_simulation.settings import DEF_RUNFILE
from AS_simulation.utils import LogTrack

import pandas as pd


def add_fasta_quant(transcriptome_fpath, transcripts_quant):
    for line in open(transcriptome_fpath):
        if line.startswith('>'):
            line = line.strip()
            tid = line.lstrip('>')
            line = '{}${}\n'.format(line, transcripts_quant.get(tid, 0))
        yield(line)


def main():
    description = 'Returns command to simulate reads using rlsim and simNGS'

    # Create arguments
    parser = argparse.ArgumentParser(description=description)
    input_group = parser.add_argument_group('Input')
    help_msg = 'Transcriptome fasta file with quantifications obtained with '
    help_msg += 'simulate_transcripts_quant'
    input_group.add_argument('fasta_quant', help=help_msg)

    options_group = parser.add_argument_group('RNA-seq options')
    options_group.add_argument('-p', '--paired_end', default=False,
                               action='store_true',
                               help='Simulate paired end reads')
    options_group.add_argument('-g', '--in_memory', default=False,
                               action='store_true',
                               help='Keep temporary files in memory')
    options_group.add_argument('-n', '--million_reads', default=20, type=float,
                               help='Million fragments to simulate (20M)')
    help_msg = 'TPM multiplier for library simulation. It can be used to '
    help_msg += 'modulate library complexity'
    options_group.add_argument('-m', '--rlsim_multiplier', default=1,
                               type=float, help=help_msg)
    options_group.add_argument('-l', '--read_length', default=100,
                               help='Read length (100)', type=int)
    options_group.add_argument('-b', '--strand_bias', default=0.5,
                               help='Strand bias (0.5)', type=float)
    options_group.add_argument('-r', '--runfile', default=DEF_RUNFILE,
                               help='simNGS runfile (s_8_4x.runfile)')

    output_group = parser.add_argument_group('Output')
    output_group.add_argument('-o', '--output', required=True,
                              help='Output fastq prefix')

    # Parse arguments
    parsed_args = parser.parse_args()
    fasta_quant_fpath = parsed_args.fasta_quant
    million_reads = parsed_args.million_reads
    rlsim_multiplier = parsed_args.rlsim_multiplier
    paired_end = parsed_args.paired_end
    in_memory = parsed_args.in_memory
    read_length = parsed_args.read_length
    strand_bias = parsed_args.strand_bias
    out_fpath = parsed_args.output
    runfile = parsed_args.runfile

    # Call rlsim
    cmd = ['rlsim']
    if in_memory:
        cmd.extend(['-g'])
    cmd.extend(['-n', str(int(million_reads * 1e6)),
           '-m', str(rlsim_multiplier), '-b', str(strand_bias), fasta_quant_fpath, '|',
           'simNGS', '-o', 'fastq', '-O', out_fpath, '-n',
           str(read_length), runfile])
    if paired_end:
        cmd.extend(['-p', 'paired'])
    sys.stdout.write(' '.join(cmd) + '\n')


if __name__ == '__main__':
    main()
