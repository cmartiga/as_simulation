#!/usr/bin/env python
import argparse

import pandas as pd

from AS_simulation.expression import simulate_txs_quant, get_tx_data
from AS_simulation.utils import LogTrack, parse_gff, load_genome


def load_optional_data(psi_fpath, tpms_fpath, alternative_fpath, log):
    if psi_fpath is None:
        psi = None
    else:
        log.write('Loading PSIs from {}'.format(psi_fpath))
        psi = pd.read_csv(psi_fpath, index_col=0)['psi'].to_dict()

    if tpms_fpath is None:
        tpms = None
    else:
        log.write('Loading TPMs from {}'.format(tpms_fpath))
        tpms = pd.read_csv(tpms_fpath, index_col=0)['tpms'].to_dict()
    
    if alternative_fpath is None:
        alternative_exons = None
    else:
        log.write('Loading alternative exons from {}'.format(alternative_fpath))
        alternative_exons = set([line.strip()
                                 for line in open(alternative_fpath)])
    return(psi, tpms, alternative_exons)


def write_tx_data(results, transcripts_fhand, quant_fhand, exon_psi_fhand):
    for gene_data in results:
        # Store transcript data
        for record in get_tx_data(gene_data):
            line = '>{}${}\n{}\n'.format(record['tx_id'],
                                         record['tpm'], record['seq'])
            transcripts_fhand.write(line)
            line = '{},{},{}\n'.format(record['tx_id'], record['tpm'],
                                       record['psi'])
            quant_fhand.write(line)
    
        # Store exon data
        for exon_id, psi, psi_mod in zip(gene_data['exon_ids'],
                                         gene_data['exon_psi'],
                                         gene_data['exon_psi_mod']):
            line = '{},{},{},{},{}\n'.format(exon_id, psi, psi_mod,
                                             gene_data['gene_id'],
                                             len(gene_data['exons']))
            exon_psi_fhand.write(line)


def main():
    description = 'Simulates transcript quantifications based on a '
    description += 'certain distribution of exon inclusion rates. It uses'
    description += 'annotation created by get_SE_transcripts.py script'

    # Create arguments
    parser = argparse.ArgumentParser(description=description)
    input_group = parser.add_argument_group('Input')
    input_group.add_argument('genome', help='Genome fasta file')
    input_group.add_argument('GxF',
                             help='GTF/GFF file with transcript annotation')

    psi_group = parser.add_argument_group('PSI options')
    psi_group.add_argument('-m', '--logit_mean', default=5, type=float,
                           help='Mean of exon logit-PSI (5)')
    psi_group.add_argument('-s', '--logit_sd', default=3, type=float,
                           help='Standard deviation of exon logit-PSI (3)')
    help_msg = 'Max number of exons that can be skipped in a transcript'
    help_msg += ' simultaneously (3)'
    psi_group.add_argument('--max_skipped', default=3, type=int,
                           help=help_msg)
    psi_group.add_argument('--max_alternative', default=3, type=int,
                           help='Max number of alternative exons per gene (3)')
    psi_group.add_argument('--p_alternative', default=0.1, type=float,
                           help='Proportion of alternative exons (0.1)')
    
    help_msg = 'Use pre-set exon PSIs from this file. Missing '
    help_msg += 'exons are assumed to be constitutive.'
    psi_group.add_argument('--psi', default=None, help=help_msg)
    help_msg = 'Force exon ids in this file to be alternatively spliced in the'
    help_msg += ' simulated files. '
    psi_group.add_argument('--alternative', default=None, help=help_msg)

    tpms_group = parser.add_argument_group('TPMs options')
    tpms_group.add_argument('-mu', '--log_mean', default=3, type=float,
                            help='Mean of exon log-TPMs (3)')
    tpms_group.add_argument('-sigma', '--log_sd', default=1, type=float,
                            help='Standard deviation of log-TPMs (1)')
    help_msg = 'Use pre-set gene TPMs from this file. Expression from missing'
    help_msg += ' genes is assumed to be 0.'
    tpms_group.add_argument('--tpms', default=None, help=help_msg)

    output_group = parser.add_argument_group('Output')
    output_group.add_argument('-q', '--quantification', required=True,
                              help='Output quantification file')
    output_group.add_argument('-e', '--exon_quant', required=True,
                              help='Output exon quantification file')
    output_group.add_argument('-t', '--transcriptome_fasta', required=True,
                              help='Output transcripts fasta file')

    # Parse arguments
    parsed_args = parser.parse_args()
    gff_fpath = parsed_args.GxF
    quant_fpath = parsed_args.quantification
    transcripts_fpath = parsed_args.transcriptome_fasta
    genome_fpath = parsed_args.genome
    
    p_alternative = parsed_args.p_alternative
    logit_mean = parsed_args.logit_mean
    logit_sd = parsed_args.logit_sd
    log_mean = parsed_args.log_mean
    log_sd = parsed_args.log_sd
    max_skipped = parsed_args.max_skipped
    max_alternative = parsed_args.max_alternative
    
    psi_fpath = parsed_args.psi
    exon_psi_fpath = parsed_args.exon_quant
    tpms_fpath = parsed_args.tpms
    alternative_fpath = parsed_args.alternative

    # Run
    log = LogTrack()
    log.write('Loading genome from {}'.format(genome_fpath))
    genome = load_genome(genome_fpath)

    psi_dict, tpms, alternative_exons = load_optional_data(psi_fpath, tpms_fpath,
                                                           alternative_fpath, log) 

    log.write('Loading annotation from {}'.format(gff_fpath))
    sep = '=' if gff_fpath.split('.')[-1] in ['gff', 'gff3'] else ' '
    with open(gff_fpath) as fhand:
        parsed_gff = parse_gff(fhand, sep=sep)
    log.write('Loaded data from {} genes'.format(len(parsed_gff)))

    # Prepare output files
    log.write('Generating quantifications')
    transcripts_fhand = open(transcripts_fpath, 'w')
    log.write('\tSaving transcript sequences at {}'.format(transcripts_fpath))
    quant_fhand = open(quant_fpath, 'w')
    quant_fhand.write('transcript,tpm,psi\n')
    log.write('\tSaving transcript sequences at {}'.format(quant_fpath))
    exon_psi_fhand = open(exon_psi_fpath, 'w')
    exon_psi_fhand.write('exon_id,psi,psi_mod,gene_id,n_exons\n')
    log.write('\tSaving exon PSIs at {}'.format(exon_psi_fpath))

    results = simulate_txs_quant(parsed_gff, genome, log_mean=log_mean,
                                 log_sd=log_sd, logit_mean=logit_mean,
                                 logit_sd=logit_sd, tpms=tpms,
                                 psi_dict=psi_dict, max_skipped=max_skipped,
                                 max_alternative=max_alternative,
                                 p_alternative=p_alternative,
                                 alternative_exons=alternative_exons)
    write_tx_data(results, transcripts_fhand, quant_fhand, exon_psi_fhand)
    log.finish()


if __name__ == '__main__':
    main()
