# Generate transcript annotation and fasta file
get_SE_transcripts genome.fa annotation.gtf -o processed_annotation.gff -t  transcripts.fa

# Simulate exon PSIs and their correspondint transcript quantifications
simulate_transcripts_quant genome.fa annotation.gtf -q transcripts_quant.csv -t transcripts_quant.fasta -e exon_psi.csv  -m 3 -s 1 --p_alternative 0.2 -mu 5 -sigma 2 --max_skipped 4 --tpms gene_tpms.csv

# Create command to simulate reads
simulate_reads transcripts_quant.fasta -n 0.01 -l 75 -p -o reads > sim_reads.sh

# Simulate reads
bash sim_reads.sh
