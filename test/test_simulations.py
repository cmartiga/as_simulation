#!/usr/bin/env python
import unittest
import random
from subprocess import check_call
from tempfile import NamedTemporaryFile
from os.path import join

import numpy as np
import pandas as pd

from AS_simulation.psi import (exon_psi_to_txs_psi, tx_psi_to_exon_psi,
                               simulate_exon_psi)
from AS_simulation.utils import parse_gff, load_genome
from AS_simulation.counts import get_AS_reads
from AS_simulation.settings import TEST_DATA_DIR, BIN_DIR
from AS_simulation.expression import sim_txs_quant, simulate_txs_quant


class SimulationsTests(unittest.TestCase):
    def test_simulate_psi(self):
        np.random.seed(0)
        random.seed(0)
        psi = simulate_exon_psi(0, 1, 0.2, 20)
        assert(psi.shape[0] == 20)
        assert(np.sum(psi != 1).sum() == 4)
        
        # Ensure max alternative works
        psi = simulate_exon_psi(0, 1, 1, 20, max_alternative=3)
        assert(psi.shape[0] == 20)
        assert(np.sum(psi != 1).sum() == 3)
        
        # Ensure setting the alternative index works
        psi = simulate_exon_psi(0, 1, 0, 20, alternative_idxs=[5, 10])
        for i, p in enumerate(psi):
            if i in [5, 10]: 
                assert(p < 1)
            else:
                assert(p == 1)
        
        # Test with a single exon gene
        psi = simulate_exon_psi(0, 1, 0, 1)
        assert(psi == 1)
    
    def test_simulate_AS_data(self):
        psi = simulate_exon_psi(0, 1, 0.5, 20)
        inclusion, total = get_AS_reads(expected_counts=20, psi=psi)
        assert(total.mean() < 40 and total.mean() > 5)
        assert(inclusion.shape == (20, 1))
        assert(total.shape == (20, 1))
        
    def test_exon_psi_to_tx_psi(self):
        # Simple case with few exon skipping events 
        exon_psi = np.array([1, 0.6, 1, 1, 0.5, 1])
        txs, psis = exon_psi_to_txs_psi(exon_psi, max_skipped=3)
         
        expected_psis = [0.3, 0.2, 0.3, 0.2]
        assert(np.allclose(psis, expected_psis))
         
        derived_exon_psi = tx_psi_to_exon_psi(txs, psis)
        assert(np.allclose(exon_psi, derived_exon_psi))
        
        # More complex case with more skipping events
        exon_psi = np.array([1, 0.6, 0.9, 0.9, 0.9, 0.5, 1])
        txs, psis = exon_psi_to_txs_psi(exon_psi, max_skipped=6)
        derived_exon_psi = tx_psi_to_exon_psi(txs, psis)
        assert(psis.sum() == 1)
        assert(np.allclose(exon_psi - derived_exon_psi, 0, atol=1e-3))
        
        # Problems when having few skipping combinations: artificially increased 
        exon_psi = np.array([1, 0.6, 1, 1, 0.5, 1])
        txs, psis = exon_psi_to_txs_psi(exon_psi, max_skipped=1)
        derived_exon_psi = tx_psi_to_exon_psi(txs, psis)
        assert(np.allclose([1, 0.8, 1, 1, 0.7, 1], derived_exon_psi))
    
    def test_generate_tx_quant_single_gene(self):
        gtf_fpath = join(TEST_DATA_DIR, 'annotation.gtf')
        gene_id = 'ENSMUSG00000058881' # 6 exons
        exon_ids = ['18:82910878-82910992+', '18:82955523-82957449+',
                    '18:82986743-82988198+', '18:82993069-82993189+',
                    '18:82994469-82994537+', '18:83001272-83005314+']
        
        exon_psi = [1, 1, 0.3709434, 1,     0.25483894, 1]
        psi_dict = dict(zip(exon_ids, exon_psi))
        tx_psi = [0.3709434 * 0.25483894, 0.25483894 * (1 - 0.3709434),
                  0.3709434 * (1 - 0.25483894), (1 - 0.3709434) * (1 - 0.25483894)]
        
        with open(gtf_fpath) as fhand:
            gtf = parse_gff(fhand, sep=' ')
            gene_data = gtf[gene_id]
            tx_quant = sim_txs_quant(gene_data, logit_mean=0, logit_sd=1,
                                     p_alternative=0.2, max_skipped=3,
                                     psi_dict=psi_dict)
            derived_exon_psi = tx_psi_to_exon_psi(tx_quant['txs'],
                                                          tx_quant['tx_psi'])
            assert(np.allclose(derived_exon_psi - tx_quant['exon_psi'], 0, atol=1e-3))
            assert(np.sum(tx_quant['tx_psi']) == 1)
            assert(np.allclose(tx_psi, tx_quant['tx_psi']))
            assert(np.allclose(exon_psi, tx_quant['exon_psi']))
            
            # Forcing alternative exons
            alternative_exons = set(['18:82986743-82988198+'])
            tx_quant = sim_txs_quant(gene_data, logit_mean=0, logit_sd=1,
                                     p_alternative=0, max_skipped=3,
                                     alternative_exons=alternative_exons)
            assert(tx_quant['exon_psi'][2] < 1)
            assert(np.all(tx_quant['exon_psi'][[0, 1, 3, 4, 5]] == 1))
    
    def test_generate_tx_quant_full(self):
        gtf_fpath = join(TEST_DATA_DIR, 'annotation.gtf')
        genome_fpath = join(TEST_DATA_DIR, 'genome.fa')
        genome = load_genome(genome_fpath)
        with open(gtf_fpath) as fhand:
            gtf = parse_gff(fhand, sep=' ')
            list(simulate_txs_quant(gtf, genome))
            
    def test_generate_tx_quant_bin(self):
        gtf_fpath = join(TEST_DATA_DIR, 'annotation.gtf')
        genome_fpath = join(TEST_DATA_DIR, 'genome.fa')
        bin_fpath = join(BIN_DIR, 'simulate_transcripts_quant.py')
        
        with NamedTemporaryFile('w') as fhand:
            tx_fpath = '{}.tx.csv'.format(fhand.name)
            exon_fpath = '{}.exon.csv'.format(fhand.name)
            fasta_fpath = '{}.fasta.csv'.format(fhand.name)

            # Ensure there is no modification of PSIs when max_alt is low                   
            cmd = [bin_fpath, genome_fpath, gtf_fpath, '-q', tx_fpath,
                   '-e', exon_fpath, '-t', fasta_fpath]
            check_call(cmd)
            exon_psi = pd.read_csv(exon_fpath)
            assert(np.allclose(exon_psi['psi'], exon_psi['psi_mod']))
             
            # Check that PSI mod is increased when limiting max_skipped
            cmd = [bin_fpath, genome_fpath, gtf_fpath, '-q', tx_fpath,
                   '-m', '0', '-s', '1',
                   '-e', exon_fpath, '-t', fasta_fpath, 
                   '--max_alternative', '4', '--max_skipped', '2']
            check_call(cmd)
            exon_psi = pd.read_csv(exon_fpath)
            exon_psi = exon_psi.loc[exon_psi['psi'] < 1, :]
            assert(not np.allclose(exon_psi['psi'], exon_psi['psi_mod']))
            assert(np.mean(exon_psi['psi'] <= exon_psi['psi_mod']) > 0.95)
             
            # Recovered when increasing max_skipped to max_alternative
            cmd = [bin_fpath, genome_fpath, gtf_fpath, '-q', tx_fpath,
                   '-m', '0', '-s', '1',
                   '-e', exon_fpath, '-t', fasta_fpath, 
                   '--max_alternative', '4', '--max_skipped', '4']
            check_call(cmd)
            exon_psi = pd.read_csv(exon_fpath)
            assert(np.allclose(exon_psi['psi'], exon_psi['psi_mod']))
             
            # Check force alternative
            alternative_fpath = join(TEST_DATA_DIR, 'alternative_ids.txt')
            cmd = [bin_fpath, genome_fpath, gtf_fpath, '-q', tx_fpath,
                   '-e', exon_fpath, '-t', fasta_fpath,
                   '--alternative', alternative_fpath]
            check_call(cmd)
             
            with open(alternative_fpath) as alt_fhand:
                alternative_ids = [line.strip() for line in alt_fhand]
                exon_psi = pd.read_csv(exon_fpath, index_col=0)
                assert(np.all(exon_psi.loc[alternative_ids, 'psi'] < 1))
            
            # Check specification of PSIs by user
            psi_fpath = join(TEST_DATA_DIR, 'exon_psi.csv')
            cmd = [bin_fpath, genome_fpath, gtf_fpath, '-q', tx_fpath,
                   '-e', exon_fpath, '-t', fasta_fpath,
                   '--psi', psi_fpath]
            check_call(cmd)
            # There may be problems in this approach with duplicated ids
            psi1 = pd.read_csv(psi_fpath).drop_duplicates('exon_id', keep=False).set_index('exon_id')
            psi2 = pd.read_csv(exon_fpath).drop_duplicates('exon_id', keep=False).set_index('exon_id')
            assert(np.allclose(psi1['psi'], psi2['psi']))

if __name__ == '__main__':
    import sys;sys.argv = ['', 'SimulationsTests']
    unittest.main()
