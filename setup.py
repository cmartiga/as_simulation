#!/usr/bin/env python

from setuptools import setup, find_packages
from AS_simulation.settings import VERSION


def main():
    description = 'Simulation of alternative splicing data'
    setup(
        name='AS_simulation',
        version=VERSION,
        description=description,
        author_email='cmarti@cnic.es',
        url='https://bitbucket.org/cmartiga/AS_simulation',
        packages=find_packages(),
        include_package_data=True,
        entry_points={
            'console_scripts': [
                'get_SE_transcripts = bin.get_SE_transcripts:main',
                'simulate_reads = bin.simulate_reads:main',
                'simulate_transcripts_quant = bin.simulate_transcripts_quant:main',
            ]},
        install_requires=['numpy', 'pandas', 'scipy', 'pysam'],
        platforms='ALL',
        keywords=['bioinformatics', 'simulations',
                  'alternative splicing', 'RNA-Seq'],
        classifiers=[
            "Programming Language :: Python :: 3",
            'Intended Audience :: Science/Research',
            "License :: OSI Approved :: MIT License",
            "Operating System :: OS Independent",
        ],
    )
    return


if __name__ == '__main__':
    main()
